#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include <iostream>
#include "Object3D.h"
#include "Plane.h"
#include <stack>
#include "GLFW\glfw3.h"
#include <vector>
class vec3{
public: 
	

	
float   x,
		y,
		z;
		
};

class Application {
public:
	std::vector<vec3> mv;
	static const int WIDTH = 512;
	static const int HEIGHT = 512;
	static const int RGB = 3;
	static const int RGBA = 4;

	Application();
	~Application();
	int crearHash(int dx, int dy);
	void init();
	int R, G, B, A;
	void display();
	void reshape(int w, int h);
	void keyboard(int key, int scancode, int action, int mods);
	void update();
	void swapbuffers();
	void cursor_position(double xpos, double ypos);
	void putPixel(GLubyte *buffer, int x, int y, unsigned char R, unsigned char G, unsigned char B, unsigned char A);
	void putPixel(int x, int y, unsigned char R, unsigned char G, unsigned char B, unsigned char A);
	void putPixel(int x, int y);
	void draw();
	void linea(int x0, int y0, int x, int y);
	void moveTo(int x, int y);
	void lineTo(int x, int y);
	void setColor(int _r, int _g, int _b, int _a);
	void setUp();
	void putFigura(const int lado, int radio);
	
	
	std::stack<glm::mat4> mStack;
	glm::mat4 mProjectionMatrix, mTransform;
	glm::vec3 vEye;

	GLFWwindow* window;
	unsigned char Buffers[2][WIDTH*HEIGHT*RGBA];
	void putLine(const unsigned int & x, const unsigned int & y, const unsigned int & x1, const unsigned int & y1);
private:
	GLuint texturesID[2], pboID[2];
	GLuint shaderID;
	GLuint VAO, VBO;
	GLuint sampler;
	GLuint uTransform;
	GLubyte* _screenBuffer;
	int _currentBuffer,
		_nextBuffer;

	glm::vec3 myLightPosition;
	GLuint uMyLightPosition[2];

	Plane oPlane;
	float fTime;
	void initTextures();
	void processPBO();
	void initPBOs();
	void updatePixels(GLubyte *buffer);
	int _drawMode,
		shader;
	bool moveLight;
	int r, g, b,a;
	int				cx,
					cy;
	void fakeBuffers();
};

#endif //__APPLICATION_H__